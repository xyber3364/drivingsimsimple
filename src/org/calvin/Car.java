package org.calvin;

public class Car {
	private static final float MPG=28f;
	
	private float gallonsOfGas;
	private int odometer;
	
	public float getGallonsOfGas() {
		return gallonsOfGas;
	}

	public void setGallonsOfGas(int gallonsOfGas) {
		this.gallonsOfGas = gallonsOfGas;
	}
	
	public int getodometer(){
		return odometer;
	}
	public void drive(float speedInMPH,float timeInHours){
		float distanceInMiles=speedInMPH*timeInHours;
		float requiredGallons=distanceInMiles/MPG;
		
		if(requiredGallons <= gallonsOfGas){
			odometer += distanceInMiles;
			gallonsOfGas -= requiredGallons;
		}
		else
		{
			float canTravelInMiles = gallonsOfGas *MPG;
			odometer += canTravelInMiles;
			gallonsOfGas=0;
		}
		
		if(gallonsOfGas == 0){
			System.out.println("I am out of gas");
		}
	}
	
	@Override
	public String toString() {
		return "Car ("+this.hashCode()+")" + " Gallons: " + gallonsOfGas + " Odometer: " + odometer;
	}

}
