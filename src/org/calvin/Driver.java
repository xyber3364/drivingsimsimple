package org.calvin;

import java.util.Random;

public class Driver {

	public static void main(String args[]) {
		Car car = new Car();
		
		System.out.println(car.toString());
		System.out.println("Filling the car with 20 gallons of gas.");
		
		car.setGallonsOfGas(20);
		
		System.out.println(car.toString());
		
		Random rand = new Random();
		while(car.getGallonsOfGas() > 0)
		{
			float speed = rand.nextFloat() * 80;
			float time = rand.nextFloat() * 3;
			
			System.out.println("Going to drive for " + time + " hours at " + speed + " mph");
			car.drive(speed, time);
			System.out.println(car.toString());
		}
	}
	
}
